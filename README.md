# 编译Windows版本

## 1.安装msys2

到[msys2官网](https://www.msys2.org/)下载安装最新的64位版本，安装完成后打开mingw-32bit窗口，执行以下命令安装依赖：

`pacman -S autoconf automake pkg-config libtool binutils gcc git make mingw-w64-i686-toolchain mingw-w64-i686-libusb mingw-w64-i686-hidapi mingw-w64-i686-libftdi`

## 2.下载源码

下载本项目的所有代码到msys2的某个目录下：

`git clone --recursive https://gitee.com/liangkangnan/tinyriscv-openocd.git`

## 3.编译源码

### 3.1第一次编译或者重新编译

在项目的根目录下，执行以下命令：

`./rebuild.sh`

### 3.2打包openocd.exe

编译完成后就可以打包了，执行以下命令进行打包：

`./release.sh`

打包后的文件在根目录下的release目录里，可以把整个release目录拷贝到电脑的其他目录下使用。

### 3.3修改源码后编译

修改源码后，只需要执行make命令即可编译。

`make`

编译完可以按照步骤3.2进行打包。

