#!/bin/bash

set -e

BASE=`pwd`

NPROC=${NPROC:-$((`nproc`*2))}
PARALLEL=${PARALLEL:--j${NPROC}}

echo "building openocd......"

./bootstrap
./configure --build=i686-w64-mingw32 --host=i686-w64-mingw32 --disable-gccwarnings CFLAGS="-O2"

sed -i '$d' ./src/jtag/drivers/libjaylink/config.h
sed -i '$d' ./src/jtag/drivers/libjaylink/config.h
sed -i '$d' ./src/jtag/drivers/libjaylink/config.h

make ${PARALLEL}
